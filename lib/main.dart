import "package:flutter/material.dart";
import 'package:testapp/view_controllers/detail.dart';
import './view_controllers/login.dart';

void main() => runApp(new MyTestApp());

class MyTestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // debugShowCheckedModeBanner: false,
        title: "Test App",
        home: Scaffold(
            // appBar: AppBar(title: Text("My First App Screen"),), // hide navigation bar
            body: LoginScreen()));
  }
}
