import 'package:flutter/material.dart';

import 'home.dart';

class Detail extends StatefulWidget {
  final int selectdIndex;
  Detail({Key key, @required this.selectdIndex}) : super(key: key);
  @override
  _DetailPageState createState() => _DetailPageState();
}

// UI customization for app information
class _DetailPageState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    var selectedApp = apps[widget.selectdIndex];
    // print(selectedApp.name);
    var white = Colors.white;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF18D191),
          title: Text("App Detail"),
        ),
        body: Container(
            padding: EdgeInsets.only(left: 10.0, top: 40.0),
            alignment: Alignment.center,
            color: white,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AppImageAsset(),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    '${selectedApp.name}',
                    style: TextStyle(fontSize: 24),
                  ),
                ),
                Expanded(
                  child: getListView(selectedApp),
                ),
              ],
            )));
  }
}

class AppImageAsset extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('lib/images/mobile.png');
    Image image = Image(
      image: assetImage,
      width: 100.0,
      height: 100.0,
    );
    return Container(
      child: image,
    );
  }
}

Widget getListView(appDetail) {
  var listView = Padding(
      padding: const EdgeInsets.only(left: 40, right: 40),
      child: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.landscape),
            title: Text('App version: ${appDetail.info_version}'),
          ),

          ListTile(
            leading: Icon(Icons.laptop_chromebook),
            title: Text('App package: ${appDetail.info_package}'),
          ),

          ListTile(
            leading: Icon(Icons.landscape),
            title: Text('App description: ${appDetail.description}'),
            onTap: () {},
          ),

          ListTile(
            leading: Icon(Icons.laptop_chromebook),
            title: Text('App version_code: ${appDetail.info_version_code}'),
          ),
          //
          SizedBox(height: 40),
          Row(
            children: <Widget>[
              RaisedButton(
                  color: Colors.deepOrange,
                  child: Text(
                    "Install",
                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                  ),
                  elevation: 6.0,
                  onPressed: () {}),
              SizedBox(
                width: 100,
              ),
              RaisedButton(
                  color: Colors.blueGrey,
                  child: Text(
                    "Open",
                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                  ),
                  elevation: 6.0,
                  onPressed: () {}),
            ],
          ),
        ],
      ));
  return listView;
}

class App {
  final int id;
  final String name;
  final String type;
  final int status;
  final int updated_at;
  final int created_at;
  final int project_id;
  final String description;
  final String image;
  final String project_name;

  final String info_id;
  final String info_name;
  final String info_package;
  final String info_version;
  final String info_file_name;
  final String info_file_path;
  final String info_custom_url;
  final int info_version_code;

  App(
      this.id,
      this.name,
      this.type,
      this.status,
      this.updated_at,
      this.created_at,
      this.project_id,
      this.description,
      this.image,
      this.project_name,
      this.info_id,
      this.info_name,
      this.info_package,
      this.info_version,
      this.info_file_name,
      this.info_file_path,
      this.info_custom_url,
      this.info_version_code);
}
