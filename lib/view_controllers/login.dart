import 'dart:core';
import 'package:flutter/material.dart';
import 'package:testapp/view_controllers/home.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Map<String, String> headers = {"content-type": "text/json"};
Map<String, String> cookies = {};

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextEditingController emailControler = new TextEditingController();
    TextEditingController passwordControler = new TextEditingController();
    final signInURl = 'https://api.testapp.io/v1/signin';

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Color(0xFF18D191))),
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 80.0),
                  child: Text(
                    "Test App",
                    style: TextStyle(fontSize: 30.0),
                  ),
                )
              ],
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
              child: TextField(
                  controller: emailControler,
                  decoration: InputDecoration(labelText: 'Email'),
                  keyboardType: TextInputType.emailAddress),
            ),
            SizedBox(
              height: 15.0,
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
              child: TextField(
                controller: passwordControler,
                obscureText: true,
                decoration: InputDecoration(labelText: 'Password'),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 80.0),
                    child: GestureDetector(
                      onTap: () async {
                        createPost(signInURl, context, emailControler.text,
                            passwordControler.text);
                      },
                      child: Container(
                          alignment: Alignment.center,
                          height: 60.0,
                          decoration: BoxDecoration(
                              color: Color(0xFF18D191),
                              borderRadius: BorderRadius.circular(9.0)),
                          child: Text("Login",
                              style: TextStyle(
                                  fontSize: 20.0, color: Colors.white))),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Post {
  final String email;
  final String password;
  Post({this.email, this.password});
  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      email: json['email'],
      password: json['password'],
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["email"] = email;
    map["password"] = password;
    return map;
  }
}

void createPost(
    String url, BuildContext context, String email, String password) async {
  Map data = {
    'email': 'dev@testapp.io', //email,
    'password': '123' //password
  };
  //encode Map to JSON
  var body = json.encode(data);
  print(body);

  var response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: body);
  updateCookie(response);
  if (response.statusCode == 200) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => HomeVC(headers: headers)));
  }
}

void updateCookie(http.Response response) {
  String allSetCookie = response.headers['set-cookie'];
  if (allSetCookie != null) {
    var setCookies = allSetCookie.split(',');
    for (var setCookie in setCookies) {
      var cookies = setCookie.split(';');
      for (var cookie in cookies) {
        _setCookie(cookie);
      }
    }
    headers['cookie'] = _generateCookieHeader();
  }
}

String _generateCookieHeader() {
  String cookie = "";
  for (var key in cookies.keys) {
    if (cookie.length > 0) cookie += ";";
    cookie += key + "=" + cookies[key];
  }
  return cookie;
}

void _setCookie(String rawCookie) {
  if (rawCookie.length > 0) {
    var keyValue = rawCookie.split('=');
    if (keyValue.length == 2) {
      var key = keyValue[0].trim();
      var value = keyValue[1];
      // ignore keys that aren't cookies
      if (key == 'path' || key == 'expires') return;
      cookies[key] = value;
    }
  }
}
