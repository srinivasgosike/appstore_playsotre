import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:testapp/view_controllers/detail.dart';
import 'dart:convert';

import 'package:testapp/view_controllers/login.dart';

// void main() => runApp(new HomeVC());
List<App> apps = [];

class HomeVC extends StatefulWidget {
  var headers = {};

  HomeVC({Key key, this.title, @required this.headers}) : super(key: key);
  // HomeVC({Key key, this.title}) : super(Key key);
  final String title;

  @override
  State<StatefulWidget> createState() => MyList();
}

Future<List<App>> getUser() async {
  var data = await http.get('https://api.testapp.io/v1/apps?platform=ios',
      headers: headers);
  var jsonDataDic = json.decode(data.body);
  print("$jsonDataDic");
  var jsonDataArray = jsonDataDic["results"];
  apps = [];
  if (jsonDataArray == null) {
    return apps;
  }
  var jsonData = jsonDataArray[0]["data"];

  // var jsonData = [];
  for (var app in jsonData) {
    App ap = App(
        app["id"],
        app["name"],
        app["type"],
        app["status"],
        app["updated_at"],
        app["created_at"],
        app["project_id"],
        app["description"],
        app["image"],
        app["project_name"],
        app["file_info"]["id"],
        app["file_info"]["name"],
        app["file_info"]["package"],
        app["file_info"]["version"],
        app["file_info"]["file_name"],
        app["file_info"]["file_path"],
        app["file_info"]["custom_url"],
        app["file_info"]["version_code"]);

    apps.add(ap);
  }
  return apps;
}

class MyList extends State<HomeVC> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF18D191),
        title: Text("App List"),
      ),
      body: Container(
        child: FutureBuilder(
          future: getUser(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: Text("Loading.."),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int indext) {
                  var versionNumber = snapshot.data[indext].info_version_code;
                  return Padding(
                      padding: new EdgeInsets.all(10.0),
                      child: new Card(
                        child: new Column(
                          children: <Widget>[
                            new ListTile(
                              leading: AppLogoAsset(),
                              title: new Text(snapshot.data[indext].name),
                              subtitle: new Text(
                                  "Version : ${snapshot.data[indext].info_version}"),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Detail(
                                            selectdIndex: indext,
                                          )),
                                );
                              },
                              trailing: btn(),
                            )
                          ],
                        ),
                      ));
                },
              );
            }
          },
        ),
      ),
    );
  }
}

class AppLogoAsset extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('lib/images/mobile.png');
    Image image = Image(
      image: assetImage,
      width: 40.0,
      height: 40.0,
    );
    return Container(
      child: image,
    );
  }
}

class btn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        color: Colors.deepOrange,
        child: Text(
          "Install",
          style: TextStyle(fontSize: 18.0, color: Colors.white),
        ),
        elevation: 6.0,
        onPressed: () {
          debugPrint("Install Clicked");
        });
  }
}

class App {
  final int id;
  final String name;
  final String type;
  final int status;
  final int updated_at;
  final int created_at;
  final int project_id;
  final String description;
  final String image;
  final String project_name;

  final String info_id;
  final String info_name;
  final String info_package;
  final String info_version;
  final String info_file_name;
  final String info_file_path;
  final String info_custom_url;
  final int info_version_code;

  App(
      this.id,
      this.name,
      this.type,
      this.status,
      this.updated_at,
      this.created_at,
      this.project_id,
      this.description,
      this.image,
      this.project_name,
      this.info_id,
      this.info_name,
      this.info_package,
      this.info_version,
      this.info_file_name,
      this.info_file_path,
      this.info_custom_url,
      this.info_version_code);
}
